package edu.udo.cs.sopra.ntf.messages

import edu.udo.cs.sopra.ntf.entity.Animal
import tools.aqua.bgw.net.common.GameAction
import tools.aqua.bgw.net.common.annotations.GameActionClass

/**
 * Network message for communicating about shuffling wildlife tokens
 * Message will be sent after the end of Overpopulation (Game Phase 1 of Game Rules)
 *
 * @param shuffledWildlifeTokens The list of all shuffled wildlife tokens
 */
@GameActionClass
data class ShuffleWildlifeTokensMessage(
    val shuffledWildlifeTokens: List<Animal>
) : GameAction()
