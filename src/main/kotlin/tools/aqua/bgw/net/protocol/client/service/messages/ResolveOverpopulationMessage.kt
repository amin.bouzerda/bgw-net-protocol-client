package edu.udo.cs.sopra.ntf.messages

import tools.aqua.bgw.net.common.GameAction
import tools.aqua.bgw.net.common.annotations.GameActionClass

/**
 * Network message for communicating about resolving the overpopulation
 * Will be sent after every ResolveOverpopulation-Trial (of three or four)
 */
@GameActionClass
class ResolveOverpopulationMessage(
) : GameAction()
