package edu.udo.cs.sopra.ntf.entity

enum class Animal {
    BEAR,
    ELK,
    SALMON,
    HAWK,
    FOX,
}