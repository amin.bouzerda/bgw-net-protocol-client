package edu.udo.cs.sopra.ntf.messages

import edu.udo.cs.sopra.ntf.entity.Animal
import edu.udo.cs.sopra.ntf.entity.ScoringCards
import tools.aqua.bgw.net.common.GameAction
import tools.aqua.bgw.net.common.annotations.GameActionClass


/**
 * Network message data to communicate the initial game state
 *
 * @param habitatTileList A List of Integers, representing the Tiles
 * @param playerList A List of Strings with the player names
 * @param gameRules A Map of Animals with [Animal] as Keys and String-values, representing Scoring Cards
 * @param startTiles A List of Integers, representing the start Tiles corresponding Tile-IDs
 * startTiles are represented like 1 -> 10, 11, 12, ...; 2 -> 20, 21, 22, ...; 3 -> 30, 31, 32, ...
 * the index is corresponding to the players id
 * @param initWildlifeTokens: A list of [Animal] entities
 */
@GameActionClass
data class GameInitMessage (
    val habitatTileList: List<Int>,
    val playerList: List<String>,
    val gameRules: Map<Animal, ScoringCards>,
    val startTiles: List<Int>,
    val initWildlifeTokens: List<Animal>,
) : GameAction()