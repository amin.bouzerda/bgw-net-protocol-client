package edu.udo.cs.sopra.ntf.messages

import edu.udo.cs.sopra.ntf.entity.Animal
import tools.aqua.bgw.net.common.GameAction
import tools.aqua.bgw.net.common.annotations.GameActionClass


/**
 * Network message for communicating about swapping by using a nature token
 * Will only be sent in second case (see game rules)
 *
 * @param swappedSelectedTokens The tokens selected for swapping
 */
@GameActionClass
data class SwappedWithNatureTokenMessage(
    val swappedSelectedTokens: List<Int>,
    val swappedWildlifeTokens: List<Animal>
) : GameAction()
