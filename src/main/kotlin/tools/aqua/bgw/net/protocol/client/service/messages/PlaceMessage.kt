package edu.udo.cs.sopra.ntf.messages

import edu.udo.cs.sopra.ntf.entity.Animal
import tools.aqua.bgw.net.common.GameAction
import tools.aqua.bgw.net.common.annotations.GameActionClass

/**
 * @property placedTile: The index of the selected HabitatTile in the comboArea
 * @property qcoordTile: the q-Coordinate for the placed Tile
 * @property rcoordTile: the r-Coordinate for the placed Tile
 * @property selectedToken: the index of the selected token from the combo-Area
 * @property qcoordToken: the qCoordinate of the habitat tile, where the token is placed on
 * @property rcoordToken: the rCoordinate of the habitat tile, where the token is placed on
 * @property usedNatureToken: if it was a natureToken select
 * @property tileRotation: Denotes how many times the tile was rotated clockwise, with 0 being the original orientation.
 *           -> Ex. 1 -> Edge 5 is upper right. 2 -> Edge 4 is upper right.
 * @property wildlifeTokens: the stack of the tiles
 */
@GameActionClass
data class PlaceMessage(
    val placedTile: Int,
    val qcoordTile: Int,
    val rcoordTile: Int,
    val selectedToken: Int,
    val qcoordToken: Int?,
    val rcoordToken: Int?,
    val usedNatureToken: Boolean,
    val tileRotation: Int,
    val wildlifeTokens: List<Animal>,
): GameAction()
