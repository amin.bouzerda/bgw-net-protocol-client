import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.7.21"
  application
  jacoco
  id("io.gitlab.arturbosch.detekt") version "1.18.0-RC3"
  id("org.jetbrains.dokka") version "1.4.32"
}

group = "edu.udo.cs.sopra"
version = "1.0"

repositories {
  mavenCentral()
  maven { url = uri("https://oss.sonatype.org/content/repositories/snapshots") }
}

application {
  mainClass.set("tools.aqua.bgw.net.protocol.client.main.MainKt")
}

dependencies {
  implementation(group = "tools.aqua", name = "bgw-gui", version = "0.9-4-06a99c3-SNAPSHOT")
  implementation(group = "tools.aqua", name = "bgw-net-common", version = "0.9")
  implementation(group = "tools.aqua", name = "bgw-net-client", version = "0.9")
}

tasks.distZip {
  archiveFileName.set("distribution.zip")
  destinationDirectory.set(layout.projectDirectory.dir("public"))
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
}

